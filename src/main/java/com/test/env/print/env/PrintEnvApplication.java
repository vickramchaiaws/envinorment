package com.test.env.print.env;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

@SpringBootApplication
@RestController
@Slf4j
public class PrintEnvApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrintEnvApplication.class, args);
    }

    @GetMapping("list-env")
    public void listEnv() {
        log.info("The System environment variables are :-" );
        Map<String, String> env = System.getenv();

        LinkedHashMap<String, String> collect =
                env.entrySet().stream()
                        .sorted(Map.Entry.comparingByKey())
                        .collect(toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (oldValue, newValue) -> oldValue, LinkedHashMap::new));

//        collect.forEach((k, v) -> log.info(k + "=" + v));
        log.info(collect.toString());
    }

    @Bean
    public ApplicationRunner init(@Autowired Environment environment) {
        return new ApplicationRunner() {
            @Override
            public void run(ApplicationArguments args) throws Exception {
                Map<String, String> env = System.getenv();

                LinkedHashMap<String, String> collect =
                        env.entrySet().stream()
                                .sorted(Map.Entry.comparingByKey())
                                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue,
                                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));

                collect.forEach((k, v) -> log.info(k + "=" + v));
            }
        };
    }

}



